function [JMLSF,lnL] = cEstBatch_joint_resample(cdynJMLS, u_arr,y_arr,prior, N_resamp)

% Grab dimensions
[~,N] = size(u_arr);

% Set prior
zp = prior.z;
lnwp = prior.lnw;
mup = prior.mu;
cPp = prior.cP;

JMLSF.pdist(1).z = zp;
JMLSF.pdist(1).lnw = lnwp;
JMLSF.pdist(1).mu = mup;
JMLSF.pdist(1).cP = cPp;
    

lnL = 0;
% Run the JMLS filter over the timeseries
for k = 1:N
    u = u_arr(:,k);
    ym = y_arr(:,k);

    % Run JMLS filter
    [lnwf,muf,cPf,zf,lnwp,mup,cPp,zp, lnLk] = HSET.dynJMLS_SS.cFilter(lnwp,mup,cPp,zp,cdynJMLS,ym,u);
    
    lnL = lnL + lnLk;
    
%     
    idx = HSET.misc.sample_lnw(N_resamp,lnwp);
    lnwp = -log(N_resamp)*ones(N_resamp,1);
    zp = zp(idx);
    mup = mup(:,idx);
    cPp = cPp(:,:,idx);
    % KL Reduction on prediction distribution
%     [zp, lnwp, mup, cPp] = HSET.HGM.cKLR(zp,lnwp, mup, cPp, KLR_Mu);

    % Save Filtered distribution
    JMLSF.dist(k).z = zf;
    JMLSF.dist(k).lnw = lnwf;
    JMLSF.dist(k).mu = muf;
    JMLSF.dist(k).cP = cPf;
    
    % Save predicted distribution
    JMLSF.pdist(k+1).z = zp;
    JMLSF.pdist(k+1).lnw = lnwp;
    JMLSF.pdist(k+1).mu = mup;
    JMLSF.pdist(k+1).cP = cPp;
end