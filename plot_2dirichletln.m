function plot_2dirichletln(alpha)


Bln = sum(gammaln(alpha))-gammaln(sum(alpha));


% B = prod(gamma(alpha))/gamma(sum(alpha));

[x1,x2] = meshgrid(linspace(0,1,1000),linspace(0,1,1000));

z = nan(size(x1));

for i = 1:length(x1(:))
%    x = [x1(i);x2(i)];
    x3 = 1 - x1(i) - x2(i);
    if x3 >= 0
        resln = (alpha(1)-1)*log(x1(i)) + (alpha(2)-1)*log(x2(i))+ (alpha(3)-1)*log(x3);
%         res = x1(i)^(alpha(1)-1) * x2(i)^(alpha(2)-1)* x3^(alpha(3)-1);
        resln = resln-Bln;
        
        z(i) = exp(resln);
%         z(i) =  1/B * res;
    end
end

surf(x1,x2,z)
xlabel('T_{1x}')
ylabel('T_{2x}')