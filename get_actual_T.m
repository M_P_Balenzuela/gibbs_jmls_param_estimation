function T = get_actual_T(z_traj,nz)
    T = zeros(nz);
    for k=1:length(z_traj)-1
        zk = z_traj(k);
        zkp1 = z_traj(k+1);
        T(zkp1,zk) = T(zkp1,zk) + 1;
    end
    
    T = T ./sum(T,1);
end