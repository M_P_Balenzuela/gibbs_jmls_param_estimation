function plot_2dirichlet(alpha)

B = prod(gamma(alpha))/gamma(sum(alpha));

[x1,x2] = meshgrid(linspace(0,1,1000),linspace(0,1,1000));

z = nan(size(x1));

for i = 1:length(x1(:))
%    x = [x1(i);x2(i)];
    x3 = 1 - x1(i) - x2(i);
    if x3 >= 0
        res = x1(i)^(alpha(1)-1) * x2(i)^(alpha(2)-1)* x3^(alpha(3)-1);
        z(i) =  1/B * res;
    end
end

surf(x1,x2,z)
xlabel('T_{11}')
ylabel('T_{21}')

