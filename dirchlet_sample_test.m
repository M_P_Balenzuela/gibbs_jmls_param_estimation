alpha = [0.3;0.6;0.1]*1000
N = 10^5;
data = nan(3,N);
for i =1:N
    data(:,i) = sample_dirichlet(alpha);
end
sample_avg = mean(data,2)