function [x,y] = histogram_pdf_points(data)

h = figure;
elf = histogram(data,'Normalization','pdf');


y = elf.Values;
x = elf.BinEdges(1:end-1) + elf.BinWidth/2;
close(h)