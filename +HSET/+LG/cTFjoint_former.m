function [cPs,mus,lnws] = cTFjoint_former(cPf, muf,lnwf, cLc, sc, rc,A,b,cQ,lnTij)



persistent struct_UT struct_UT_TRANSA
if isempty(struct_UT) % if one is empty, they both will be
   struct_UT = struct('UT',true);
   struct_UT_TRANSA = struct('UT',true,'TRANSA',true);   
end

nx = size(sc,1);
nxln2pi = log(2*pi);
nxln2pi = nx*nxln2pi;
twonxln2pi = 2*nxln2pi;

 
    
minusmup = [muf;A*muf+b]; % do this as a vectorised operation  
minusmup = -minusmup;

% qr([A 0;
%     B C])

% Pp is for the joint prediction density [xk xk+1]
cPp = [cPf cPf*A.';zeros(nx) cQ];
      
% Pp*sp = -mup      
tmp = linsolve(cPp,minusmup,struct_UT_TRANSA);
sp = linsolve(cPp,tmp,struct_UT);

ss = sp;
ss(nx+1:end) = ss(nx+1:end) + sc;

tp = 2*lnwf;
tp = -tp;

tp = tp + twonxln2pi;
tmps = HSET.misc.TrLn(cPp);

tmps = 2*tmps;
tp = tp + tmps;

tmp = cPp*sp;
tmps = tmp.'*tmp;
tp = tp + tmps;

ts = tp + rc;
tmps = 2*lnTij;
ts = ts - tmps;

% cL = gamma
R2 = triu(qr([eye(nx*2) zeros(nx*2);
    zeros(2*nx,nx) cPp(:,nx+1:end)*cLc.' cPp]));
      
cPs = R2(nx*2+1:end,nx*2+1:end);

tmp = cPs*ss;
mus = -cPs.'*tmp;

lnws = tmp.'*tmp;
lnws = lnws - ts;
lnws = 0.5*lnws;
lnws = lnws + nxln2pi;

tmps = HSET.misc.TrLn(cPs);
lnws = lnws + tmps;


