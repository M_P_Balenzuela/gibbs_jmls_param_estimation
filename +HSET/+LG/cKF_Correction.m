function [muf,cPf,lnL] = cKF_Correction(mup,cPp,C,d_minus_ym,cR)

persistent struct_UT 
if isempty(struct_UT) 
   struct_UT = struct('UT',true);
end

% Get dimms
[ny,nx] = size(C);

% Correction step
QR2 = [cR         zeros(ny,nx);
      cPp*C.'    cPp];
  
% Q-less QR decomp.
R2 = triu(qr(QR2));

R2_11 = R2(1:ny,1:ny); % sq of CPC'+R
R2_12 = R2(1:ny,ny+1:end); % (CPC'+R)^(-T/2) CP
cPf = R2(ny+1:end,ny+1:end); % R2_22 is sq of P_{k|k}

% KT = R2_11\R2_12;
KT = linsolve(R2_11,R2_12,struct_UT);

yerr = C*mup+d_minus_ym;
muf = mup - KT.'*yerr;

if nargout > 2
    lnL = HSET.Gaussian.clnLikelihood(yerr,R2_11);
end