function cdynLG = to_c(dynLG)

    cdynLG.A = dynLG.A;
    
    if isfield(dynLG,'B')
        cdynLG.B = dynLG.B;
    end
    
    C = dynLG.C;
    cdynLG.C = C;
    
    if isfield(dynLG,'D')
        cdynLG.D = dynLG.D;
    end
    
    Q = dynLG.Q;
    R = dynLG.R;
    if isfield(dynLG,'S')
        S = dynLG.S;
    else
       [ny,nx] = size(C); 
       S = zeros(nx,ny); 
    end
    
    cdynLG.cPi = chol([R S.';
                    S Q]);
end