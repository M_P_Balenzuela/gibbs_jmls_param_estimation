function cdynJMLS = to_c(dynJMLS)

nz = dynJMLS.N;
cdynJMLS.N = nz;
cdynJMLS.lnT = dynJMLS.lnT;
for m = 1:nz
    
    cdynJMLS.model(m) = HSET.dynLG_SS.to_c(dynJMLS.model(m));
end
end
