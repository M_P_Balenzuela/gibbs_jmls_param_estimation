function [xtrue,ztrue,yidx] = simulate(x0,dynJMLS,uidx,z0)

% Grab dims.
[~,N] = size(uidx);
[ny,nx] = size(dynJMLS.model(1).C);

% if d0 is a scaler, or if d0 is a discrete traj.
if isscalar(z0)
    ztrue = nan(N,1);
    z = z0;
    ztrue(1) = z;
    for k=2:N
%         z = randsample(dynJMLS.N,1,true,exp(dynJMLS.lnT(:,z)) );
        z = HSET.misc.sample_lnw(1,dynJMLS.lnT(:,z),'multinomial');
        ztrue(k) = z;
    end
else
    ztrue = z0;
end


xtrue = nan(nx,N+1);

yidx = nan(ny,N);

% Load initial condition
x = x0;

xtrue(:,1) = x0;

 

for k = 1:N
    u = uidx(:,k);
    
    z = ztrue(k);
    SS = dynJMLS.model(z);
    
    [x,y] = HSET.dynLG_SS.simulate(x,SS,u);
    x = x(:,2);
    xtrue(:,k+1) = x;
    yidx(:,k) = y;    
	
end