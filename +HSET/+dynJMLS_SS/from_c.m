function dynJMLS = from_c(cdynJMLS)

nz = cdynJMLS.N;
dynJMLS.N = nz;
dynJMLS.lnT = cdynJMLS.lnT;
for m = 1:nz
    
    dynJMLS.model(m) = HSET.dynLG_SS.from_c(cdynJMLS.model(m));
end
end


