function M = cCombined_expectation(cPl, mul, lnwl, u, ym)
% E[xk;xk+1;uk;yk][.]^T ^0.5

persistent struct_UT_TRANSA 
if isempty(struct_UT_TRANSA) 
    struct_UT_TRANSA = struct('UT',true,'TRANSA',true);   % is the argument UT, does it need to be transposed?
    %struct_UT = struct('UT',true);
end

two_nx = size(cPl,1); % MAKE SURE THIS IS A SQUARE MATRIX!!

QR1 = [cPl; mul.'];

M11 = triu(qr(QR1));
M11 = M11(1:two_nx,1:two_nx);

lambda = linsolve(M11, mul,struct_UT_TRANSA);
vT = [u.' ym.'];
M12 = lambda*vT;
M22 = sqrt(1-lambda.'*lambda)*vT;

M = [M11 M12;
    zeros(1,two_nx) M22];

scale = exp(0.5*lnwl);
M = scale*M;