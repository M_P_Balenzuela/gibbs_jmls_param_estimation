/* Lightweight implementation for sampling from a non-parametric PMF, defined by log-weight components
   Copyright Mark P. Balenzuela 2019 
   No stealing :)

% IN:
	% N - Number of indices required
	% lnw(:) - Column vector of log weights
	% Chosen method (optional) 'multinomial'/'stratified'/'systematic' (default)

% OUT: 
    % Idx(:) - Column vector of indices
*/


#include "mex.h"
#include "math.h"
#include <string.h>



double LSE_pair(double a, double b)
{
	if ((a==-mxGetInf())&&(b==-mxGetInf())) // to prevent to appearence of nans
	{
		return(a);
	}
	
    if (a<b)
    {
        // swap a and b
        double c = a;
        a = b;
        b = c;
    }
	// assuming a is larger
	b = b-a;
	b = exp(b);
	b = b + 1.0;
	b = log(b);
	a = a + b;
	return(a);        
        
}


// Generate random number
double my_rand()
{
	/*
	mxArray *randomnum[1];

mexCallMATLAB(1, randomnum, 0,  NULL, "rand");
double *lnw    = mxGetPr(randomnum[0]);
return(*lnw);
*/
    //int Randbits = (int) (log(RAND_MAX)/log(2));
    if (RAND_MAX <32000)
    {
        mexErrMsgIdAndTxt("MyProg:ConvertString","SYSTEM UNSUPPORTED, RAND_MAX is %d", RAND_MAX);
    }
	

    double d1 = rand();
    double d2 = rand();
    


	double Rmaxp1 = (double) RAND_MAX+1.0;
    double dout =  d1 + Rmaxp1*d2;// + RAND_MAX*RAND_MAX*d3;
    dout = dout / ((double)(Rmaxp1*Rmaxp1)-1.0);

	//mexPrintf("%lf\n%lf\n",d1,d2); 
	
    return(dout);
}


// This: clnw=log(cumsum(exp(lnw)))
void cumsum_lnw(int M, double *lnw, double *clnw)
{
    int i, j;
    
    clnw[0] = lnw[0];
    double maxlnw = lnw[0];
    double expdiff = 1.0; //exp(0);//sum_j=1^i exp(lnw[j] - lnwmax)
    for (i=1;i<M;++i)
    {
        if (lnw[i] > maxlnw)
        {
            maxlnw = lnw[i];
            expdiff = 0;
            for (j=0;j<i;++j)// = 1:i ...   recompute the expdiff
            {
                expdiff += exp(lnw[j] - maxlnw);   
            }
        }
        expdiff += exp(lnw[i] - maxlnw);
        // LSE
        clnw[i] = maxlnw + log(expdiff);
        
        // Note: we could sort them to ensure we find the max first, but that leads to other issues with log(LSB)? Also requires computational effort to sort
        
                
    }
    
}



// Have M, want N, pick from lnw to get idx
void multinomial_resampling(int N, int M, double *lnw, double *idx)
{
    int i, j;
    
    double *clnw;
    
    clnw = (double *)malloc(M*sizeof(double));
    cumsum_lnw(M, lnw, clnw);
    
    for (i=0;i<N;++i)
    {
        double lnrand = log(my_rand());
        
        for (j=0;j<M;++j)
        {

            if ((clnw[j]>=lnrand)) // ||(j==(M-1))
            {
                idx[i] = (double) (j+1.0); // remove the +1 for MEX functions, this is to accomodate matlab
                break;
            }
			if (j==(M-1))
			{
				mexPrintf("lost boy: %lf, maxcum: %lf\n", lnrand, clnw[j]);
				idx[i] = (double) (j+1.0); // remove the +1 for MEX functions, this is to accomodate matlab
                break;
			}
        }
        
    }
    
    
    free(clnw);
    
}




// Have M, want N, pick from lnw to get idx
void stratified_resampling(int N, int M, double *lnw, double *idx)
{
    int i, j;
    
    double *clnw;
    
    clnw = (double *)malloc(M*sizeof(double));
    cumsum_lnw(M, lnw, clnw);
    
    double logN = -log((double) N);
    
     
    int lastj = 0;
    for (i=0;i<N;++i)
    {
       
        double lnrand = log(my_rand()) + logN;
        lnrand = LSE_pair(lnrand, logN+log((double) i) );
        for (j=lastj;j<M;++j)
        {
            
            
            if ((clnw[j]>=lnrand)||(j==(M-1)))
            {
                idx[i] = (double) (j+1.0); // remove the +1 for MEX functions, this is to accomodate matlab
                lastj = j;
                break;
            }
            
        }
        
    }
    
    
    free(clnw);
    
}




// Have M, want N, pick from lnw to get idx
void systematic_resampling(int N, int M, double *lnw, double *idx)
{
    int i, j;
    
    double *clnw;
    
    clnw = (double *)malloc(M*sizeof(double));
    cumsum_lnw(M, lnw, clnw);
    
    double logN = -log((double) N);
    double lnrand = log(my_rand()) + logN;
     
    int lastj = 0;
    for (i=0;i<N;++i)
    {
       
        
        for (j=lastj;j<M;++j)
        {
            if ((clnw[j]>=lnrand)||(j==(M-1)))
            {
                idx[i] = (double) (j+1.0); // remove the +1 for MEX functions, this is to accomodate matlab
                lnrand = LSE_pair(lnrand,logN);
                lastj = j;
                break;
            }
            
        }
        
    }
    
    
    free(clnw);
    
}





/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
/*
% IN:
	% N - Number of indices required
	% lnw(:) - Column vector of log weights
	% Chosen method (optional) 'multinomial'/'stratified'/'systematic' (default)

% OUT: 
    % Idx(:) - Column vector of indices
*/

    if (nrhs < 2)
    {
        mexErrMsgTxt("Wrong number of input dimension, should be atleast 2!");
    }
	
        
    // Get input pointers	
    int N = (int) *mxGetPr(prhs[0]);
    double *lnw    = mxGetPr(prhs[1]);
    int M = mxGetM(prhs[1]);
    
    
    

    // Allocate space for result
    plhs[0] = mxCreateDoubleMatrix(N,1, mxREAL);
    double *idx = mxGetPr(plhs[0]);
	

    if (nrhs == 3)
    {
        if (!(mxIsChar(prhs[2]))){
            mexErrMsgIdAndTxt( "MATLAB:mexatexit:invalidInput",
                    "3rd input argument must be of type string.\n");
        }
            
        char *opts = mxArrayToString(prhs[2]);
        
        if (0==strcmp(opts, "systematic"))
        {
            systematic_resampling(N, M, lnw, idx);
        }

        else if (0==strcmp(opts, "multinomial"))
        {
            multinomial_resampling(N, M, lnw, idx);
        }

        else if (0==strcmp(opts, "stratified"))
        {       
            stratified_resampling(N, M, lnw, idx);
        }
        else
        {
            mexErrMsgTxt("Invalid sampling strategy, must be:  systematic, multinomial, or stratified!\n");
        }
        
        // clear opts string
        mxFree(opts);
    }
    else
    {
        systematic_resampling(N, M, lnw, idx); // Default method
    }

    
    
}