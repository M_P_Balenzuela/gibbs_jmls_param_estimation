function [ii,jj,min_val] = minMat(A)
% Find the matrix element with the lowest value

[inter_B, inter_i] = min(A);
[min_val, jj] = min(inter_B);
ii = inter_i(jj);