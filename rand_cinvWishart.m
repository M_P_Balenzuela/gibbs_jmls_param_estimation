function M = rand_cinvWishart(cLambda,nu)
% df must be an integer for bartlett decomp.
% Modified from iwishrnd built-in procedure
% See Matrix Variate Distributions by A.K. Gupta & D.K. Nagar, 
%Theorem 3.3.1 and Theorem 3.3.4

persistent struct_UT_TRANSA 
if isempty(struct_UT_TRANSA) 
   struct_UT_TRANSA = struct('UT',true,'TRANSA',true);   
end

n = size(cLambda,2);
    

% Load diagonal elements with square root of chi-square variates
x = diag(sqrt(chi2rnd(nu-(0:n-1))));

% Load upper triangle with independent normal (0, 1) variates
x(itriu(n)) = randn(n*(n-1)/2,1);


M = linsolve(x,cLambda,struct_UT_TRANSA);

% Make UT
M = triu(qr(M));
M = M(1:n,1:n);
end

function d = itriu(p)
    d = ones(p*(p-1)/2,1);
    d(1+cumsum(0:p-2)) = p+1:-1:3;
    d = cumsum(d);
end