function plot_dirichletln_new(alpha_arr)

N = size(alpha_arr,3);
resln_arr = nan(N,1);



% B = prod(gamma(alpha))/gamma(sum(alpha));

[x1,x2] = meshgrid(linspace(0,1,400),linspace(0,1,400));


for col =1:3
    figure(col);clf;
    z = nan(size(x1));

    for i = 1:length(x1(:))
%         i
    %    x = [x1(i);x2(i)];
        x3 = 1 - x1(i) - x2(i);
        if x3 >= 0

            for j=1:N
                alpha = alpha_arr(:,col,j);
                resln = (alpha(1)-1)*log(x1(i)) + (alpha(2)-1)*log(x2(i))+ (alpha(3)-1)*log(x3);
                Bln = sum(gammaln(alpha))-gammaln(sum(alpha));
                resln_arr(j) = resln-Bln - log(N);
            end
                z(i) = exp(HSET.misc.LSE(resln_arr));
    %         z(i) =  1/B * res;
        end
    end

    surf(x1,x2,z);
    xlabel('T_{1x}');
    ylabel('T_{2x}');
    title(['Transition Column ' num2str(col)]);
end