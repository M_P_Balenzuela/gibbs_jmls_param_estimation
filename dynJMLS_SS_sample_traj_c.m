function [x_hyp,z_hyp] = dynJMLS_SS_sample_traj_c(JMLSF,cdynJMLS,u_arr,y_arr)
% sample possible trajectories from filtered distribution

N = length(u_arr);
nx = size(cdynJMLS.model(1).A,1);

idx = HSET.misc.sample_lnw(1,JMLSF.pdist(N+1).lnw,'multinomial');
cP = JMLSF.pdist(N+1).cP(:,:,idx);
xkp1 = JMLSF.pdist(N+1).mu(:,idx) + cP.'*randn(nx,1);
zkp1 = JMLSF.pdist(N+1).z(idx);

x_hyp = nan(nx,N+1);
z_hyp = nan(N+1,1);
x_hyp(:,N+1) = xkp1;
z_hyp(N+1,1) = zkp1;

for k = N:-1:1
    u = u_arr(:,k);
    ym = y_arr(:,k);
    
    % unpack structure to save time
    JMLSFk_z = JMLSF.dist(k).z;
    JMLSFk_lnw = JMLSF.dist(k).lnw;
    JMLSFk_mu = JMLSF.dist(k).mu;
    JMLSFk_cP = JMLSF.dist(k).cP;
    
    % Do KF correction as per derivation
    Mk = length(JMLSFk_lnw);
    cPc_store = nan(nx,nx,Mk);
    muc_store = nan(nx,Mk);
    lnwc_store = nan(Mk,1);
    
    zp = -1; % for initialisation, not a valid index
    for i = 1:Mk% each mode in the k-th filtered dist
        % Load correct model
        zi = JMLSFk_z(i);
        if (zi ~= zp)
            zp = zi;
            [A,b,cQ] = HSET.dynLG_SS.cUnpack(cdynJMLS.model(zi),ym,u);
            d_minus_ym = b - xkp1; 
        end
                
        muik = JMLSFk_mu(:,i);
        cPik = JMLSFk_cP(:,:,i);
        [muc_store(:,i),cPc_store(:,:,i),lnL] = HSET.LG.cKF_Correction(muik,cPik,A,d_minus_ym,cQ);
        lnwc_store(i) = lnL + JMLSFk_lnw(i) + cdynJMLS.lnT(zkp1,zi);
    end
    
    lnwc_store = HSET.misc.normalise_lnw(lnwc_store);
    idx = HSET.misc.sample_lnw(1,lnwc_store,'multinomial');
    
    cP = cPc_store(:,:,idx);
    xkp1 = muc_store(:,idx) + cP.'*randn(nx,1);
    zkp1 = JMLSFk_z(idx);
    
    x_hyp(:,k) = xkp1;
    z_hyp(k,1) = zkp1;
end

