function plot_Transition_hist(cdynJMLS_store)
burn_in = 0;
% Sturge�s Rule in 2D
N = length(cdynJMLS_store.run)-1;
K = 1+3.322*log(N-burn_in)*5;
% K = K^0.5;
K = ceil(K);
xspace = linspace(0,1,K);
[x1,x2] = meshgrid(xspace,xspace);

binwidth = 1/(K-1);

for i=1:3
    bincount = zeros(K,K);
    for j = 1+burn_in:N
        Ti_vec = exp(cdynJMLS_store.run(j+1).lnT(1:2,i));
        bins = real(Ti_vec > (0.5*binwidth+xspace(1:end-1)) );
        bin = sum(bins,2)+1;
        bin_i = (bin-[0;1]).' *[1;K];

        bincount(bin_i)= bincount(bin_i) +1;
    end
    bincount = bincount / (sum(bincount(:))*(binwidth*binwidth));
    figure(i); clf;
    T = delaunay(x1,x2);
    
    for j=1:K
            bincount(end-j+1+1+1:end,j) = nan;
    end
    
    trisurf(T,x1,x2,bincount.');
%     surf(x1,x2,bincount.');
    title(['T, Column ' num2str(i)])
    xlabel(['T_{' num2str(1) ',' num2str(i) '}'])
    ylabel(['T_{' num2str(2) ',' num2str(i) '}'])
    set(gca,'fontsize', 16);
end





