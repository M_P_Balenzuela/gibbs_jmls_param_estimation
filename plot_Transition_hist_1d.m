function plot_Transition_hist_1d(cdynJMLS_store)
burn_in = 0;
% Sturge�s Rule in 2D
N = length(cdynJMLS_store.run)-1;
% K = 1+3.322*log(N-burn_in)*5;
% K = K^0.5;
% K = ceil(K);
% xspace = linspace(0,1,K);


% binwidth = 1/(K-1);

for i=1:2
    data = nan(length(1+burn_in:N),1);
    for j = 1+burn_in:N
        data(j) = cdynJMLS_store.run(j+1).lnT(i,i);
    end
    
    data = exp(data);
    [x,y] = histogram_pdf_points(data);
    
%     
%     
%     bincount = zeros(K,1);
%     for j = 1+burn_in:N
%         Ti_vec = exp(cdynJMLS_store.run(j+1).lnT(i,i)); % First is true system
%         bins = real(Ti_vec > (0.5*binwidth+xspace(1:end-1)) );
%         bin = sum(bins)+1;
% 
%         bincount(bin)= bincount(bin) +1;
%     end
%     bincount = bincount / (sum(bincount(:))*(binwidth));
    
    figure; clf;
    
    plot(x, y, '-b','LineWidth',2)
%     plot(xspace, bincount, '-b','LineWidth',2)
    
    
    
    %     surf(x1,x2,bincount.');
    title(['Probability density of T(' num2str(i) ',' num2str(i) ')'])
    xlabel('Value') %['Probability of model ' num2str(i) ' being used consecutively']
    ylabel('Probability Density')
    set(gca,'fontsize', 14);
    
    % plot true
    hold on;
    trueT = exp(cdynJMLS_store.run(1).lnT(i,i));
    drawnow;
    yl = ylim;
    plot(trueT*ones(1,2),yl,'--r','LineWidth',2);
    legend('Distribution','True value')
end





