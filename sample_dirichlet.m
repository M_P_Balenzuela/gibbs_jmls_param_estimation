function y = sample_dirichlet(alpha)

n = length(alpha);
y = nan(n,1);
for i = 1:n
    y(i) = gamrnd(alpha(i),1);
end
y = y/sum(y);