%% Clean workspace
clc;
clear all;
close all;

struct_UT_TRANSA = struct('UT',true,'TRANSA',true);   
struct_UT = struct('UT',true);

rng(1);

%% Simulation Settings
N = 2*10^3; % Number of simulated time steps
num_Gibbs = 2*10^6;
N_resamp = 5;

%% Define a dynamic JMLS system
% Model 1          
dynJMLS.model(1).A = 0.4766;

dynJMLS.model(1).B = -1.207;
dynJMLS.model(1).Q = 0.001;
   
C = 0.4889;
D = -0.3034;
R = 0.02;

dynJMLS.model(1).C = C*dynJMLS.model(1).A;
dynJMLS.model(1).D = C*dynJMLS.model(1).B+D;
dynJMLS.model(1).R = C*dynJMLS.model(1).Q*C.'+R;
dynJMLS.model(1).S = 0;



%  Model 2
dynJMLS.model(2).A = -0.1721;
   
dynJMLS.model(2).B = 1.533;

dynJMLS.model(2).Q = 0.034;

C = 1.117;
D = 0.03256;
R = 0.0015;

dynJMLS.model(2).C = C*dynJMLS.model(2).A;
dynJMLS.model(2).D = C*dynJMLS.model(2).B+D;
dynJMLS.model(2).R = C*dynJMLS.model(2).Q*C.'+R;
dynJMLS.model(2).S = 0;




nu = size(dynJMLS.model(1).B,2);
[ny,nx] = size(dynJMLS.model(1).C);

nz = length(dynJMLS.model);
dynJMLS.N = nz;

 dynJMLS.lnT = log([0.7 0.5;
                    0.3 0.5]);

%% Simulate the system for N timesteps
x0 = zeros(nx,1);
u_arr = randn(1,N);
% u_arr = 100*sin(1:N);
zini = HSET.misc.sample_lnw(1,-ones(nz,1)*log(nz),'multinomial'); % starting model
[xtrue_arr, ztrue_arr, y_arr] = HSET.dynJMLS_SS.simulate(x0,dynJMLS,u_arr,zini);

%% Convert the system to sqrt form for numerically-stable estimation
cdynJMLS = HSET.dynJMLS_SS.to_c(dynJMLS);

%% Perform Gibbs sampling on the system
cdynJMLS_store.run(1) = cdynJMLS; % save true
tic;
% Set prior on parameters
% Set mean of parameter distribution
M(:,:,1) = zeros(nx+ny,nx+nu);
M(:,:,2) = zeros(nx+ny,nx+nu);        


% chol inv of column cov matricies        
cinvV(:,:,1) = chol(inv(13*eye(nx+nu)));      
cinvV(:,:,2) = chol(inv(13*eye(nx+nu)));

% scaling 
cLambda(:,:,1) = 10^-5*eye(nx+ny);
cLambda(:,:,2) = 10^-5*eye(nx+ny);

% degree of freedom
dof(1) =  nx+ny;
dof(2) =  nx+ny;

alpha0 = ones(nz);

% Set prior on predicted state distribution at for k=1
mu0 = x0;
cP0 = eye(nx);
[~,prior.z,prior.lnw,prior.mu,prior.cP] = HSET.GM.to_HGM(nz,0,mu0,cP0);
% Allocate space for param storage

% Do Gibbs:

plotaswego = false;
if plotaswego
    for m = 1:nz
       figure(m); 
       clf; 
       hold on;
       sys1=ss(cdynJMLS.model(m).A,cdynJMLS.model(m).B,cdynJMLS.model(m).C,cdynJMLS.model(m).D,1);
       bode(sys1,'-b');
    end
end

nE = nx*2+nu+ny;
T = nan(nz);
idx1 = 1:(nx+nu);
idx2 = (nx+nu)+1:nE;
for i = 1:num_Gibbs
    i
    % Sample hybrid state trajectory
    JMLSF = cEstBatch_joint_resample(cdynJMLS, u_arr,y_arr,prior,N_resamp);
%     JMLSF = HSET.dynJMLS_SS.cEstBatch_joint(cdynJMLS, u_arr,y_arr,prior,KLR_Mu); % USES KLR MERGING - NOT RESAMPLING
    [xtraj,ztraj] = dynJMLS_SS_sample_traj_c(JMLSF,cdynJMLS,u_arr,y_arr);
    
    % Sample new parameter set
    % Begin by calcuating expectation of sufficient stats
    expect = zeros(nE,nE,nz);
    num_m = zeros(nz,1);
%     T = zeros(nz,nz);
    alpha = alpha0;
    for k = 1:N
        zk = ztraj(k);
        zkp1 = ztraj(k+1);
        alpha(zkp1,zk) = alpha(zkp1,zk) + 1;
        vec = [xtraj(:,k);u_arr(:,k);y_arr(:,k);xtraj(:,k+1)].';
        tmp = triu(qr([vec;expect(:,:,zk) ]));
        expect(:,:,zk) = tmp(1:nE,1:nE);
        num_m(zk) = num_m(zk) + 1;
    end

    for m = 1:nz
        if num_m(m) >0
            mvcomp = [cinvV(:,:,m) cinvV(:,:,m)*M(:,:,m).'];
            tmp = triu(qr([mvcomp;expect(:,:,m) ]));
            expect(:,:,m) = tmp(1:nE,1:nE);

            cSigma_bar = expect(idx1,idx1,m);
%             cinvsigmabar_LT = (eye(nx+ny)/cSigma_bar).'; % LT now, Is a triangular solve
            cinvsigmabar_LT = linsolve(cSigma_bar,eye(nx+nu),struct_UT_TRANSA);
            

%             M_bar = (expect(idx1,idx1,m)\expect(idx1,idx2,m)).'; % Is a triangular solve
            M_bar = linsolve(expect(idx1,idx1,m),expect(idx1,idx2,m),struct_UT).';
            
            cPi_star = expect(idx2,idx2,m);


            cLambda_bar = triu(qr([cPi_star;cLambda(:,:,m)]));
            cLambda_bar = cLambda_bar(1:(nx+ny),1:(nx+ny));
            nu_bar = num_m(m) + dof(m);        
        
            % Sample Pi
            cPi = rand_cinvWishart(cLambda_bar,nu_bar);
%             Lambda_bar = cLambda_bar.'*cLambda_bar;
% %             cinvLambda_bar_LT = (eye(nx+ny)/cLambda_bar).'; % Is a triangular solve
%             cinvLambda_bar_LT = linsolve(cLambda_bar,eye(nx+ny),struct_UT_TRANSA);
%             Pi = iwishrnd(Lambda_bar,nu_bar,cinvLambda_bar_LT); % Lambda is the same as tau in doc https://au.mathworks.com/help/stats/inverse-wishart-distribution.html
%             cPi = chol(Pi);
            
            
            
            % Sample Gamma
            Gamma = M_bar + cPi.'*randn(nx+ny,nx+nu)*cinvsigmabar_LT;
%             invsigmabar = cinvsigmabar_LT.'*cinvsigmabar_LT; % not needed if sampling approach changed
%             Gamma = M_bar(:) + sqrtm(kron(invsigmabar,Pi))*randn(length(M_bar(:)),1);
%             Gamma = reshape(Gamma, nx+ny,nx+nu);
            
            
            

            % Repack Gamma and Pi    
            cdynJMLS.model(m).C = Gamma(1:ny,1:nx);
            cdynJMLS.model(m).D = Gamma(1:ny,nx+1:end);
            cdynJMLS.model(m).A = Gamma(ny+1:end,1:nx);
            cdynJMLS.model(m).B = Gamma(ny+1:end,nx+1:end);   

            cdynJMLS.model(m).cPi = cPi;

            T(:,m) = sample_dirichlet(alpha(:,m));
            

            % repeatidly ploting bodes takes forever.
            if plotaswego
                figure(m);
                sys1=ss(cdynJMLS.model(m).A,cdynJMLS.model(m).B,cdynJMLS.model(m).C,cdynJMLS.model(m).D,1);
                bode(sys1,'-b');
                drawnow;
            end
        end
    end

    cdynJMLS.lnT = log(T);
%     T
    
    % Save parameter sample
    cdynJMLS_store.run(i+1) = cdynJMLS;
    
end

Gibbs_time = toc;
disp([num2str(num_Gibbs) ' iterations completed in ' num2str(Gibbs_time) ' seconds.']);

save lastrunvtwoex1

close all
%% Plot results

% This matching operation is a bit expensive m^(num Gibbs) combinations
% need to be checked.

T_true = get_actual_T(ztrue_arr,nz)


dw = 0.01; w = 0.01:dw:pi;
timestep = 1;
[bode_mag,bode_phase, map_arr] = generate_bodes(w,timestep, cdynJMLS_store);

% remap models
TcdynJMLS_store = cdynJMLS_store;
for i = 2:length(cdynJMLS_store.run)
    map = map_arr(:,i);
    % Actually do the mapping
    for ii=1:nz
        TcdynJMLS_store.run(i).model(ii) = cdynJMLS_store.run(i).model(map(ii));
        for jj=1:nz
            TcdynJMLS_store.run(i).lnT(ii,jj) = cdynJMLS_store.run(i).lnT(map(ii),map(jj));
        end
    end

end

%%

if nz >2
    plot_Transition_hist(TcdynJMLS_store); % supports nz=3
else
    plot_Transition_hist_1d(TcdynJMLS_store);
end

%%
linW = 2;
figure(4);
clf;
a = 0.05;
[ha,~] = tight_subplot(2,1,[a/2 a]*2,[a*2 a],[a a/2]);

data_a = nan(length(cdynJMLS_store.run)-1,length(w));
data_b = nan(length(cdynJMLS_store.run)-1,length(w));
for i=1:cdynJMLS.N  % model
    for m = 1:length(cdynJMLS_store.run)-1

        a = bode_mag(:,i,m+1);
        b = bode_phase(:,i,m+1);
        
        num_rev_off = round((bode_phase(1,i,1) - b(1))/360);
        b = b + num_rev_off*360;
        data_a(m,:) = 20*log10(a).';
        data_b(m,:) = b.';
     end

        axes(ha(1))
        errhandles(i) = shadedErrorBar(w, data_a, {@mean,@std3}, 'lineprops', {'-r','LineWidth',linW});
        set(gca, 'XScale', 'log');
%         if (m==nM)
%             legend('True','Initial Guess','JMLS EM (Proposed)');
%         end

        title('Bode Diagram'); % (Model ' num2str(i) ')
        ylabel('Magnitude (dB)');
        hold on
        xlim([w(1) w(end)]);
        set(gca,'fontsize', 16);

        axes(ha(2))
        sec_errhandles(i) = shadedErrorBar(w, data_b, {@mean,@std3}, 'lineprops', {'-r','LineWidth',linW});
        set(gca, 'XScale', 'log');

        ylabel('Phase (deg)');
        xlabel('Frequency (rad/s)');
        hold on;
        xlim([w(1) w(end)]);
        
        set(gca,'fontsize', 16);
   
    
end
% Bring mean and edge lines to top
for i=1:cdynJMLS.N 
    uistack(errhandles(i).mainLine,'top');
    uistack(sec_errhandles(i).mainLine,'top');
    
    uistack(errhandles(i).edge,'top');
    uistack(sec_errhandles(i).edge,'top');
    
end
for i=1:cdynJMLS.N
        a = bode_mag(:,i,1);
        b = bode_phase(:,i,1);
        a = 20*log10((a));
        axes(ha(1));
        trueplot = plot(w,a,'--b','LineWidth',linW);
        
        axes(ha(2));
        plot(w,b,'--b','LineWidth',linW);
end

legend([trueplot, errhandles(1).mainLine, errhandles(1).patch],'True system response', 'Mean response','3 SD confidence interval');

%%

if nx==1
    burn_in = 2;
    A_str = nan(length(1+burn_in:N),nz);
    D_str = nan(length(1+burn_in:N),nz);
    R_str = nan(length(1+burn_in:N),nz);
     for j = 1+burn_in:N
         for i =1:nz
            A_str(j,i) = TcdynJMLS_store.run(j+1).model(i).A;
            D_str(j,i) = TcdynJMLS_store.run(j+1).model(i).D;

            Pii = TcdynJMLS_store.run(j+1).model(i).cPi;
            Pii = Pii.'*Pii;
            R_str(j,i) = Pii(1,1);

         end
     end

     for i =1:nz
            % Plot system A
            figure;clf;
%             elf = histogram(A_str(:,i),'Normalization','pdf');
            [xdt,ydt] = histogram_pdf_points(A_str(:,i));
            plot(xdt,ydt,'-b','LineWidth',2);
            xlim([xdt(1) xdt(end)]);
            hold on;
%             plot(dynJMLS.model(i).A,0,'rx','LineWidth',2);
            title(['Distribution of A(' num2str(i) ')'])
            xlabel('Value')
            ylabel('Probability density')
            set(gca,'fontsize', 16);
            drawnow;
            yl = ylim;
            plot(dynJMLS.model(i).A*ones(1,2),yl,'--r','LineWidth',2);
            legend('Distribution','True value')
            
            % Plot system D
            figure;clf;
%             histogram(D_str(:,i),'Normalization','pdf');
            [xdt,ydt] = histogram_pdf_points(D_str(:,i));
            plot(xdt,ydt,'-b','LineWidth',2);
            xlim([xdt(1) xdt(end)]);
            hold on;
%             plot(dynJMLS.model(i).D,0,'rx','LineWidth',2);
            title(['Distribution of D(' num2str(i) ')']);
            xlabel('Value');
            ylabel('Probability density');
            set(gca,'fontsize', 16);
            drawnow;
            yl = ylim;
            plot(dynJMLS.model(i).D*ones(1,2),yl,'--r','LineWidth',2);
            legend('Distribution','True value');
            
            % Plot system R
            figure;clf;
%             histogram(R_str(:,i),'Normalization','pdf');
            [xdt,ydt] = histogram_pdf_points(R_str(:,i));
            plot(xdt,ydt,'-b','LineWidth',2);
            xlim([xdt(1) xdt(end)]);
            hold on;
%             plot(dynJMLS.model(i).R,0,'rx','LineWidth',2);
            title(['Distribution of R(' num2str(i) ')']);
            xlabel('Value');
            ylabel('Probability density');
            set(gca,'fontsize', 16);
            drawnow;
            yl = ylim;
            plot(dynJMLS.model(i).R*ones(1,2),yl,'--r','LineWidth',2);
            legend('Distribution','True value');

     end
        
        
end

