function [lnw_f,mu_f,cP_f,z_f,lnw_p,mu_p,cP_p, z_p, LnL] = cFilter_resamp(lnwp,mup,cPp,zp,cdynJMLS,ym,u,N_resamp)

if nargin < 7
    u = zeros(0,1);
end

nz = cdynJMLS.N;
nx = size(cdynJMLS.model(1).A,1);
lnT = cdynJMLS.lnT;

Nf = length(lnwp);
z_f = zp;
lnw_f = nan(Nf,1);
mu_f = nan(nx,Nf);
cP_f = nan(nx,nx,Nf);


% z_f = nan(Np,1);

mu_p = nan(nx,Nf);
cP_p = nan(nx,nx,Nf);

zi = -1; % -1 is not an valid model, which forces the model to be loaded for first iteration
for i = 1:Nf
    
    % Don't reload the model if it is already loaded
    if (zi ~= zp(i))
        zi = zp(i);
        [A,b,cQ,C,d,cR] = HSET.dynLG_SS.cUnpack(cdynJMLS.model(zi),ym,u);
        d_minus_ym = d - ym; 
    end
    
    
    [mu_f(:,i),cP_f(:,:,i),tmp] = HSET.LG.cKF_Correction(mup(:,i),cPp(:,:,i),C,d_minus_ym,cR);
    lnw_f(i) = tmp + lnwp(i);
    
    [mu_p(:,i),cP_p(:,:,i)] = HSET.LG.cKF_Prediction(mu_f(:,i),cP_f(:,:,i),A,b,cQ);
    
end

if nargout > 4
    LnL = HSET.misc.LSE(lnw_f);
end

    Nf = N_resamp;
    idx = HSET.misc.sample_lnw(N_resamp,lnw_f);
    lnw_f = -log(N_resamp)*ones(N_resamp,1);
    zp = zp(idx);
    mu_p = mu_p(:,idx);
    cP_p = cP_p(:,:,idx);

z_p = nan(nz*N_resamp,1);

% lnw_f = HSET.misc.normalise_lnw(lnw_f);

% Discrete prediction
idx = 0;
for i =1:nz
    nidx = idx+Nf;
    z_p(idx+1:nidx) = i;
    idx = nidx;
end

tidexs = z_p+nz*(repmat(zp,nz,1)-1);
lnw_p = repmat(lnw_f,nz,1)+lnT(tidexs);
mu_p = repmat(mu_p,1,nz);
cP_p = repmat(cP_p,1,1,nz);
