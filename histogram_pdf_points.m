function [x,y] = histogram_pdf_points(data)


[y,edge] = histcounts(data,'Normalization','pdf');

% For nice plots make sure the ends are at zero
y = [0 y 0];
binwidth = edge(2)-edge(1);
edge = [edge(1)-binwidth, edge];

x = edge(1:end) + binwidth/2;